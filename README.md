Kitti-GPay

## Clone Repository

1. Git clone https://sulfa21@bitbucket.org/sulfa21/google-pay-java.git
2. Open the repository on Eclipse, go to Eclipse Marketplace, install Google Cloud Tools for Eclipse 1.8.3.
3. Follow these instructions https://cloud.google.com/appengine/docs/standard/java/building-app/environment-setup


## Deployment

1. Open GeneralConstants.java on com.google.wallet.objects.constant, change issuer_id, issuer_name, loyalty_class_entity, loyalty_object_entity, bucket_name depend on environment to deploy.
2. Right-click on the project, Deploy to App Engine Standard, log in using Kitti account, choose the project with a name : loyaltyproject and id: loyaltyproject-256603, change the version to "dev" or "prod" depend on the environment. It will be deployed to https://{{version}}-dot-loyaltyproject-256603.appspot.com/

## Access Datastore and Bucket on Google Cloud

1. Go to https://console.cloud.google.com/
2. Choose home->dashboard->resources->storage to see the list of bucket.
3. Choose datastore->entities->select kind(datastore's name) to see datastore.

Account to deploy app engine and log in to google cloud: info@kitti.com.au

## Reference

1. https://developers.google.com/pay/passes/guides/introduction/about-google-pay-api-for-passes
2. https://www.youtube.com/watch?v=z5a3dq1KEOw