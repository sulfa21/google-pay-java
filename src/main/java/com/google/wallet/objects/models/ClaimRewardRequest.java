package com.google.wallet.objects.models;
/**
 * @author sulfa
 *
 */
public class ClaimRewardRequest {
	private String objectId;
	private int rewardClaimed;
	private String productClaimed;
	private String objectKey;
	
	public String getObjectKey() {
		return objectKey;
	}
	
	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}
	
	public String getObjectId() {
		return objectId;
	}
	
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public int getRewardClaimed() {
		return rewardClaimed;
	}
	
	public void setRewardClaimed(int rewardClaimed) {
		this.rewardClaimed = rewardClaimed;
	}
	
	public String getProductClaimed() {
		return productClaimed;
	}
	
	public void setProductClaimed(String productClaimed) {
		this.productClaimed = productClaimed;
	}
}
