package com.google.wallet.objects.models;

import java.util.ArrayList;

/**
 * @author sulfa
 *
 */
public class KittiResponse {
	ArrayList<LoyaltyObjectResponse> response = new ArrayList<>();
	private String responseMessage;
	private Boolean isError;
	
	public KittiResponse(ArrayList<LoyaltyObjectResponse> response, String responseMessage, Boolean isError) {
		this.response = response;
		this.responseMessage = responseMessage;
		this.isError = isError;
	}

	public KittiResponse() {
		
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public ArrayList<LoyaltyObjectResponse> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<LoyaltyObjectResponse> response) {
		this.response = response;
	}
}
