package com.google.wallet.objects.models;

/**
 * @author sulfa
 *
 */
public class AccessRewardRequest {
	private String classKey;
	private int point;
	private int totalPoint;
	
	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getTotalPoint() {
		return totalPoint;
	}

	public void setTotalPoint(int totalPoint) {
		this.totalPoint = totalPoint;
	}

	public String getClassKey() {
		return classKey;
	}

	public void setClassKey(String classKey) {
		this.classKey = classKey;
	}
}
