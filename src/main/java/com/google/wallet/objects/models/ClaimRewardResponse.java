package com.google.wallet.objects.models;
/**
 * @author sulfa
 *
 */
public class ClaimRewardResponse {
	private String responseMessage;
	private Boolean isError;
	private String debug;

	public ClaimRewardResponse(String responseMessage, Boolean isError) {
		this.responseMessage = responseMessage;
		this.isError = isError;
	}

	public ClaimRewardResponse() {

	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
}
