package com.google.wallet.objects.models;

/**
 * @author sulfa
 *
 */
public class UpdateLoyaltyClassResponse {	
	private String responseMessage;
	private String responseCode;
	private Boolean isError;
	private String debug;
	
	public UpdateLoyaltyClassResponse(String responseMessage, Boolean isError) {
		this.responseMessage = responseMessage;
		this.isError = isError;
	}
	
	public UpdateLoyaltyClassResponse() {
		
	}
	
	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public String getResponseMessage() {
		return responseMessage;
	}
	
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	public String getResponseCode() {
		return responseCode;
	}
	
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	
	public Boolean getIsError() {
		return isError;
	}
	
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
}
