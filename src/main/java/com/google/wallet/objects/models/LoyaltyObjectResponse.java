package com.google.wallet.objects.models;

/**
 * @author sulfa
 *
 */
public class LoyaltyObjectResponse {
	private String objectId;
	private String linkLoyalty;
	private Integer currentPoints;
	private String objectKey;	
	
	public String getObjectKey() {
		return objectKey;
	}
	
	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}
	
	public Integer getCurrentPoints() {
		return currentPoints;
	}
	
	public void setCurrentPoints(Integer currentPoints) {
		this.currentPoints = currentPoints;
	}
	
	public String getObjectId() {
		return objectId;
	}
	
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public String getLinkLoyalty() {
		return linkLoyalty;
	}
	
	public void setLinkLoyalty(String linkLoyalty) {
		this.linkLoyalty = linkLoyalty;
	}
}
