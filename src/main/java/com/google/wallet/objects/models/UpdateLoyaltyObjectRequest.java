package com.google.wallet.objects.models;

/**
 * @author sulfa
 *
 */
public class UpdateLoyaltyObjectRequest {
	private String objectId;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
}
