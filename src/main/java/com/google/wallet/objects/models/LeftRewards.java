package com.google.wallet.objects.models;

import java.util.HashMap;

public class LeftRewards {
	private String leftRewards;
	private int count;
	HashMap<Integer, String> hmap = new HashMap<Integer, String>();
	private int countReward;

	public LeftRewards(String leftRewards, int count, HashMap<Integer, String> hmap, int countReward) {
		this.leftRewards = leftRewards;
		this.count = count;
		this.hmap = hmap;
		this.countReward = countReward;
	}

	public HashMap<Integer, String> getHmap() {
		return hmap;
	}

	public void setHmap(HashMap<Integer, String> hmap) {
		this.hmap = hmap;
	}

	public String getLeftRewards() {
		return leftRewards;
	}

	public void setLeftRewards(String leftRewards) {
		this.leftRewards = leftRewards;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCountReward() {
		return countReward;
	}

	public void setCountReward(int countReward) {
		this.countReward = countReward;
	}
}
