package com.google.wallet.objects.models;

import java.util.List;
import com.google.wallet.objects.utils.RewardStatus;
/**
 * @author sulfa
 *
 */
public class AccessRewardResponse {
	private String key;
	private String rewardStatus;
	List<RewardStatus> rewardStat;
	private String responseMessage;
	private Boolean isError;
	
	public AccessRewardResponse(List<RewardStatus> rewardStat, String responseMessage, Boolean isError) {
		super();
		this.rewardStat = rewardStat;
		this.responseMessage = responseMessage;
		this.isError = isError;
	}
	
	public AccessRewardResponse() {
		
	}
	
	public Boolean getIsError() {
		return isError;
	}
	
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
	
	public List<RewardStatus> getRewardStat() {
		return rewardStat;
	}
	
	public void setRewardStat(List<RewardStatus> rewardStat) {
		this.rewardStat = rewardStat;
	}
	
	public String getResponseMessage() {
		return responseMessage;
	}
	
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getRewardStatus() {
		return rewardStatus;
	}
	
	public void setRewardStatus(String rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
}
