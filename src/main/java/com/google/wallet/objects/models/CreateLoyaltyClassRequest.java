package com.google.wallet.objects.models;

import java.util.ArrayList;

import com.google.wallet.objects.utils.Location;
import com.google.wallet.objects.utils.RewardStatus;

/**
 * @author sulfa
 *
 */
public class CreateLoyaltyClassRequest {
	private String issuerName;
	private String programName;
	private String classId;
	private String headerMessage;
	private String bodyMessage;
	private String fontColor;
	private String backgroundColor;
	ArrayList<Location> location;
	private String uriLogoImage;
	private String uriHeroImage;
	private String uriHomepage;
	private String issuerId;
	private String env;
	private String notifStatus;
	private String classKey;
	private int totalPoints;
	ArrayList<RewardStatus> rewardStatus;

	
	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public String getClassKey() {
		return classKey;
	}

	public void setClassKey(String classKey) {
		this.classKey = classKey;
	}

	public ArrayList<RewardStatus> getRewardStatus() {
		return rewardStatus;
	}

	public void setRewardStatus(ArrayList<RewardStatus> rewardStatus) {
		this.rewardStatus = rewardStatus;
	}

	public String getNotifStatus() {
		return notifStatus;
	}

	public void setNotifStatus(String notifStatus) {
		this.notifStatus = notifStatus;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getUriHomepage() {
		return uriHomepage;
	}

	public void setUriHomepage(String uriHomepage) {
		this.uriHomepage = uriHomepage;
	}

	public String getUriLogoImage() {
		return uriLogoImage;
	}

	public void setUriLogoImage(String uriLogoImage) {
		this.uriLogoImage = uriLogoImage;
	}

	public String getUriHeroImage() {
		return uriHeroImage;
	}

	public void setUriHeroImage(String uriHeroImage) {
		this.uriHeroImage = uriHeroImage;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getHeaderMessage() {
		return headerMessage;
	}

	public void setHeaderMessage(String headerMessage) {
		this.headerMessage = headerMessage;
	}

	public String getBodyMessage() {
		return bodyMessage;
	}

	public void setBodyMessage(String bodyMessage) {
		this.bodyMessage = bodyMessage;
	}

	public String getFontColor() {
		return fontColor;
	}

	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public ArrayList<Location> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<Location> location) {
		this.location = location;
	}
}
