package com.google.wallet.objects.models;

/**
 * @author sulfa
 *
 */
public class CreateLoyaltyClassResponse {
	private String responseCode;
	private String responseMessage;
	private Boolean isError;
	private String classKey;

	public CreateLoyaltyClassResponse(String responseMessage, Boolean isError, String classKey) {
		this.responseMessage = responseMessage;
		this.isError = isError;
		this.classKey = classKey;
	}

	public CreateLoyaltyClassResponse() {

	}

	public String getClassKey() {
		return classKey;
	}

	public void setClassKey(String classKey) {
		this.classKey = classKey;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}
