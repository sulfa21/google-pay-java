package com.google.wallet.objects.models;

/**
 * @author sulfa
 *
 */
public class KittiRequest {
	private String classId;
	private String objectId;
	private String userId;
	private Integer points;
	private Integer totalPoints;
	private String userName;
	private Integer currentPoints;
	private String backgroundImageUrl;
	private String stampImageUrl;
	private String classKey;
	private String objectKey;
	
	public String getObjectKey() {
		return objectKey;
	}
	
	public void setObjectKey(String objectKey) {
		this.objectKey = objectKey;
	}
	
	public String getClassKey() {
		return classKey;
	}
	
	public void setClassKey(String classKey) {
		this.classKey = classKey;
	}
	
	public String getBackgroundImageUrl() {
		return backgroundImageUrl;
	}
	
	public void setBackgroundImageUrl(String backgroundImageUrl) {
		this.backgroundImageUrl = backgroundImageUrl;
	}
	
	public String getStampImageUrl() {
		return stampImageUrl;
	}
	
	public void setStampImageUrl(String stampImageUrl) {
		this.stampImageUrl = stampImageUrl;
	}
	
	public String getClassId() {
		return classId;
	}
	
	public void setClassId(String classId) {
		this.classId = classId;
	}
	
	public String getObjectId() {
		return objectId;
	}
	
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Integer getPoints() {
		return points;
	}
	
	public void setPoints(Integer points) {
		this.points = points;
	}
	
	public Integer getTotalPoints() {
		return totalPoints;
	}
	
	public void setTotalPoints(Integer totalPoints) {
		this.totalPoints = totalPoints;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Integer getCurrentPoints() {
		return currentPoints;
	}
	
	public void setCurrentPoints(Integer currentPoints) {
		this.currentPoints = currentPoints;
	}
}
