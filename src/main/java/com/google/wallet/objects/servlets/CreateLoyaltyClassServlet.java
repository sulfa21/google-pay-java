package com.google.wallet.objects.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.google.api.client.json.GenericJson;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.LoyaltyClass;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.CreateLoyaltyClassRequest;
import com.google.wallet.objects.models.CreateLoyaltyClassResponse;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobUtils;
import com.google.wallet.objects.verticals.Loyalty;

/**
 * @author sulfa
 *
 */
public class CreateLoyaltyClassServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Gson gson = new Gson();

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		CreateLoyaltyClassResponse lcresp = new CreateLoyaltyClassResponse();
		GenericJson response = null;
		try {
			String issuerId = GeneralConstants.ISSUER_ID;
			CreateLoyaltyClassRequest obj = new Gson().fromJson(req.getReader(), CreateLoyaltyClassRequest.class);

			JSONArray rewardStatusArray = getRewardStatusArray(obj);
			String classKey = getClassKey(obj, rewardStatusArray);
			lcresp.setClassKey(classKey);

			WobCredentials credentials = new WobCredentials(GeneralConstants.SERVICE_ACCOUNT_ID,
					GeneralConstants.SERVICE_ACCOUNT_PRIVATE_KEY, GeneralConstants.APPLICATION_NAME, issuerId);
			/**
			 * Create WobUilts object to handle the heavy lifting
			 */
			WobUtils utils = null;
			Walletobjects client = null;
			try {
				utils = new WobUtils(credentials);
				client = utils.getClient();
			} catch (Exception e) {
				lcresp = new CreateLoyaltyClassResponse("Failed " + e.getMessage(), true, null);
				e.printStackTrace();
			}

			String classId = obj.getClassId();
			CreateLoyaltyClassRequest loyaltyRequest = new CreateLoyaltyClassRequest();

			if (obj.getBackgroundColor() != null) {
				loyaltyRequest.setBackgroundColor(obj.getBackgroundColor());
			} else {
				loyaltyRequest.setBackgroundColor(GeneralConstants.DEFAULT_BACKGROUND_COLOR);
			}

			if (obj.getBodyMessage() != null) {
				String bodyMsg = obj.getBodyMessage(), newMsg = "";
				String[] array = null;
				if(bodyMsg.contains("Welcome")) {
					array = bodyMsg.split("[.]");
					for(int i = 1; i< array.length; i++) {
						newMsg += array[i] + ".";
					}
					newMsg = newMsg.trim();
					loyaltyRequest.setBodyMessage(newMsg);
				}else {
					loyaltyRequest.setBodyMessage(obj.getBodyMessage());
				}
			} else {
				loyaltyRequest.setBodyMessage(GeneralConstants.DEFAULT_BODY_MESSAGE);
			}

			if (obj.getFontColor() != null) {
				loyaltyRequest.setFontColor(obj.getFontColor());
			} else {
				loyaltyRequest.setFontColor(GeneralConstants.DEFAULT_FONT_COLOR);
			}
			loyaltyRequest.setClassId(classId);

			if (obj.getHeaderMessage() != null) {
				loyaltyRequest.setHeaderMessage(obj.getHeaderMessage());
			} else {
				loyaltyRequest.setHeaderMessage(GeneralConstants.DEFAULT_HEADER_MESSAGE);
			}

			loyaltyRequest.setIssuerName(obj.getIssuerName());

			if (obj.getLocation() != null) loyaltyRequest.setLocation(obj.getLocation());

			if (obj.getProgramName() != null) {
				loyaltyRequest.setProgramName(obj.getProgramName());
			} else {
				loyaltyRequest.setProgramName(GeneralConstants.DEFAULT_PROGRAM_NAME);
			}

			if (obj.getUriHeroImage() != null) loyaltyRequest.setUriHeroImage(obj.getUriHeroImage());
			
			if (obj.getUriHomepage() != null) loyaltyRequest.setUriHomepage(obj.getUriHomepage());
		
			if (obj.getUriLogoImage() != null) {
				loyaltyRequest.setUriLogoImage(obj.getUriLogoImage());
			} else {
				loyaltyRequest.setUriLogoImage(GeneralConstants.DEFAULT_URI_LOGO_IMAGE);
			}
			LoyaltyClass loyaltyClass = Loyalty.generateLoyaltyClass(loyaltyRequest);
			try {
				response = client.loyaltyclass().insert(loyaltyClass).execute();
				lcresp = new CreateLoyaltyClassResponse(GeneralConstants.MESSAGE_SUCCESS, false, classKey);
			} catch (Exception e) {
				lcresp = new CreateLoyaltyClassResponse("Failed " + e.getMessage(), true ,null);
			}
		} catch (Exception e) {
			lcresp = new CreateLoyaltyClassResponse("Failed " + e.getMessage(), true, null);
		}
		PrintWriter out = resp.getWriter();
		out.write(gson.toJson(lcresp));
	}

	/**
	 * @param obj
	 * @param rewardStatusArray
	 * @return
	 */
	private String getClassKey(CreateLoyaltyClassRequest obj, JSONArray rewardStatusArray) {
		Entity rewardStatusList = new Entity(GeneralConstants.LOYALTY_CLASS_ENTITY, obj.getClassId());
		rewardStatusList.setProperty("rewardStatus", rewardStatusArray.toString());
		rewardStatusList.setProperty("totalPoints", obj.getTotalPoints());
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		datastore.put(rewardStatusList);
		Key loyaltyClassKey = rewardStatusList.getKey();
		String classKey = KeyFactory.keyToString(loyaltyClassKey);
		return classKey;
	}

	/**
	 * @param obj
	 * @return
	 * @throws JSONException
	 */
	public static JSONArray getRewardStatusArray(CreateLoyaltyClassRequest obj) throws JSONException {
		JSONArray rewardStatusArray = new JSONArray();
		HashMap<String, JSONObject> maprewards = new HashMap<String, JSONObject>();
		if (obj.getRewardStatus() != null) {
			for (int j = 0; j < obj.getRewardStatus().size(); j++) {
				JSONObject jsonReward = new JSONObject();
				jsonReward.put("milestone", obj.getRewardStatus().get(j).getMilestone());
				jsonReward.put("status", obj.getRewardStatus().get(j).getStatus());
				ArrayList<String> listRewards = new ArrayList<String>();
				String[] lr = obj.getRewardStatus().get(j).getRewards();
				for (String x : lr) {
					listRewards.add(x);
				}
				jsonReward.put("rewards", new JSONArray(listRewards));
				maprewards.put("jsonReward" + j, jsonReward);
				rewardStatusArray.put(maprewards.get("jsonReward" + j));
			}
		}
		return rewardStatusArray;
	}
}
