package com.google.wallet.objects.servlets;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PreparedQuery.TooManyResultsException;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.api.client.json.GenericJson;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.Image;
import com.google.api.services.walletobjects.model.ImageModuleData;
import com.google.api.services.walletobjects.model.LatLongPoint;
import com.google.api.services.walletobjects.model.LoyaltyClass;
import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.api.services.walletobjects.model.LoyaltyObjectListResponse;
import com.google.api.services.walletobjects.model.LoyaltyPoints;
import com.google.api.services.walletobjects.model.LoyaltyPointsBalance;
import com.google.api.services.walletobjects.model.Uri;
import com.google.api.services.walletobjects.model.WalletObjectMessage;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.CreateLoyaltyClassRequest;
import com.google.wallet.objects.models.CreateLoyaltyClassResponse;
import com.google.wallet.objects.models.KittiResponse;
import com.google.wallet.objects.models.UpdateLoyaltyClassResponse;
import com.google.wallet.objects.utils.ImageStamp;
import com.google.wallet.objects.utils.Location;
import com.google.wallet.objects.utils.RewardStatus;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobUtils;

/**
 * @author sulfa Update Location
 *
 */
public class UpdateLoyaltyClassServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static Gson gson = new Gson();
	private DatastoreService datastore;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		UpdateLoyaltyClassResponse lcresp = new UpdateLoyaltyClassResponse();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		TransactionOptions options = TransactionOptions.Builder.withXG(true);
		Transaction txn = datastore.beginTransaction(options);
		GenericJson response = null, resplist = null;
		GenericJson responseObj = null;
		String objectId = null, availableRewardList = null, objectKey = null, rewardStatus = null, stampImageUrl = null;
				
		try {
			String issuerId = GeneralConstants.ISSUER_ID;
			CreateLoyaltyClassRequest obj = new Gson().fromJson(req.getReader(), CreateLoyaltyClassRequest.class);

			WobCredentials credentials = new WobCredentials(GeneralConstants.SERVICE_ACCOUNT_ID,
					GeneralConstants.SERVICE_ACCOUNT_PRIVATE_KEY, GeneralConstants.APPLICATION_NAME, issuerId);
			/**
			 * Create WobUilts object to handle the heavy lifting
			 */
			WobUtils utils = null;
			Walletobjects client = null;
			try {
				utils = new WobUtils(credentials);
				client = utils.getClient();
			} catch (Exception e) {
				lcresp = new UpdateLoyaltyClassResponse("Failed, FileNotFoundException, " + e.getMessage(), true);
			}

			String classId = obj.getClassId();
			LoyaltyClass loyaltyClass = client.loyaltyclass().get(issuerId + "." + classId).execute();

			/**
			 * 
			 * Update loyaltyClass property (issuerName, programName, uriHomePage)
			 */

			if (obj.getIssuerName() != null)
				loyaltyClass.setIssuerName(obj.getIssuerName());
			if (obj.getProgramName() != null)
				loyaltyClass.setProgramName(obj.getProgramName());
			if (obj.getUriHomepage() != null) {
				loyaltyClass.setHomepageUri(new Uri().setUri(obj.getUriHomepage()));
			}
			/**
			 * Update loyaltyClass property (location, uriLogoImage)
			 */
			LatLongPoint llp = new LatLongPoint();
			List<LatLongPoint> locations = new ArrayList<LatLongPoint>();
			if (obj.getLocation() != null) {
				for (Location loc : obj.getLocation()) {
					llp = new LatLongPoint();
					llp.setLatitude(loc.getLatitude());
					llp.setLongitude(loc.getLongitude());
					locations.add(llp);
				}
				loyaltyClass.setLocations(locations);
			}

			if (obj.getUriLogoImage() != null) {
				loyaltyClass.setProgramLogo(new Image().setSourceUri(new Uri().setUri(obj.getUriLogoImage())));
			}

			List<WalletObjectMessage> messagesClass = new ArrayList<WalletObjectMessage>();
			WalletObjectMessage messageClass = new WalletObjectMessage();
			
			if (obj.getHeaderMessage() != null) {
				messageClass.setHeader(obj.getHeaderMessage());
			}else {
				messageClass.setHeader(loyaltyClass.getMessages().get(0).getHeader());
			}
				
			if (obj.getBodyMessage() != null) {
				messageClass.setBody(obj.getBodyMessage());
			}else {
				messageClass.setBody(loyaltyClass.getMessages().get(0).getBody());
			}
			
			messagesClass.add(messageClass);
			loyaltyClass.setMessages(messagesClass);
			
			/**
			 * Update loyaltyClass property(version and reviewStatus) --> mandatory
			 */
			loyaltyClass.setVersion(loyaltyClass.getVersion() + 1L);
			loyaltyClass.setReviewStatus("underReview");

			if (obj.getRewardStatus() != null) {
				List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
				JSONArray rewardStatusArray = CreateLoyaltyClassServlet.getRewardStatusArray(obj);
				
				Key classKey = KeyFactory.stringToKey(obj.getClassKey());
				Entity got = datastore.get(classKey);
				got.setProperty("rewardStatus", rewardStatusArray.toString());
				datastore.put(got);
			}
			response = client.loyaltyclass().update(loyaltyClass.getId(), loyaltyClass).execute();
			txn.commit();
			lcresp.setIsError(false);
			lcresp.setResponseMessage(GeneralConstants.MESSAGE_SUCCESS);
		} catch (Exception e) {
			lcresp.setIsError(true);
			lcresp.setResponseMessage("Failed ," + e.getMessage());
		}finally {
			  if (txn.isActive()) {
			    txn.rollback();
			 }
		}
		String respon = new Gson().toJson(lcresp, UpdateLoyaltyClassResponse.class);
		PrintWriter out = resp.getWriter();
		out.write(respon);
	}
}
