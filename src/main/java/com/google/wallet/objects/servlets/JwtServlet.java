package com.google.wallet.objects.servlets;

import java.io.FileNotFoundException;
import org.apache.commons.lang3.ArrayUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import com.google.api.client.json.GenericJson;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;
import java.io.PrintWriter;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.security.KeyStoreException;
import java.security.SignatureException;
import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.Image;
import com.google.api.services.walletobjects.model.ImageModuleData;
import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.api.services.walletobjects.model.LoyaltyPoints;
import com.google.api.services.walletobjects.model.LoyaltyPointsBalance;
import com.google.api.services.walletobjects.model.Uri;
import com.google.api.services.walletobjects.model.WalletObjectMessage;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.KittiRequest;
import com.google.wallet.objects.models.KittiResponse;
import com.google.wallet.objects.models.LoyaltyObjectResponse;
import com.google.wallet.objects.utils.ImageStamp;
import com.google.wallet.objects.utils.ObjectData;
import com.google.wallet.objects.utils.RewardStatus;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobPayload;
import com.google.wallet.objects.utils.WobUtils;
import com.google.wallet.objects.verticals.Loyalty;

/**
 * @author sulfa
 *
 */
public class JwtServlet extends HttpServlet {

	/**
	 * The purpose of this servlet is to create a card (loyalty object) by
	 * generating jwt and also update an existing card based on objectId
	 */
	private static final String LINK_SAVE_CARD = "https://pay.google.com/gp/v/save/";
	private static final String STAMP_IMAGE_NOT_FOUND = "Stamp image not found";
	private static final String OBJECT_ID_NOT_FOUND = "Object Id not found";
	private final long serialVersionUID = 1L;
	private static Gson gson = new Gson();
	private static final Random generator = new Random();

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		KittiResponse resp2 = new KittiResponse();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		String availableRewardList = null;
		try {

			KittiRequest obj = new Gson().fromJson(req.getReader(), KittiRequest.class);
			String classKey = obj.getClassKey();
			int rand = 0;
			if (obj.getStampImageUrl() == null) {
				throw new Exception(STAMP_IMAGE_NOT_FOUND);
			}
			String backgroundImage = null;
			if (obj.getBackgroundImageUrl() != null)
				backgroundImage = obj.getBackgroundImageUrl();

			String stampImage = obj.getStampImageUrl();

			String userId = obj.getUserId();
			if (userId.contains("@")) {
				userId = obj.getUserId().replace('@', 'x');
			}

			String issuerId = GeneralConstants.ISSUER_ID;
			WobCredentials credentials = new WobCredentials(GeneralConstants.SERVICE_ACCOUNT_ID,
					GeneralConstants.SERVICE_ACCOUNT_PRIVATE_KEY, GeneralConstants.APPLICATION_NAME, issuerId);
			WobUtils utils2 = null;
			Walletobjects client = null;
			/**
			 * Instantiate the WobUtils class which contains lots of handy functions
			 */
			try {
				utils2 = new WobUtils(credentials);
				client = utils2.getClient();
			} catch (Exception e) {
				resp2 = new KittiResponse(null, "Failed, " + e.getMessage(), true);
			}
			/**
			 * Add valid domains for the Save to Wallet button
			 */
			List<String> origins = new ArrayList<String>();
			origins.add("http://localhost:8080");
			origins.add("https://localhost:8080");
			origins.add("http://loyaltyproject-256603.appspot.com");
			origins.add("https://loyaltyproject-256603.appspot.com");

			WobPayload payload = new WobPayload();
			String jwt = null, link = null, stampImageUrl = null;
			GenericJson response = null;
			LoyaltyObjectResponse loyaltyObjectResponse = new LoyaltyObjectResponse();
			ArrayList<LoyaltyObjectResponse> loyaltyResp = new ArrayList<>();

			if (obj.getObjectId() != null) {
				LoyaltyObject loyaltyObj = client.loyaltyobject().get(issuerId + "." + obj.getObjectId()).execute();
				if (loyaltyObj.size() < 0 || loyaltyObj.getState().equalsIgnoreCase(GeneralConstants.STATE_INACTIVE)) {
					throw new Exception(OBJECT_ID_NOT_FOUND);
				}
				Integer poin = loyaltyObj.getLoyaltyPoints().getBalance().getInt();
				if (poin == obj.getCurrentPoints()) {
					poin = poin + obj.getPoints();
				} else {
					poin = obj.getCurrentPoints() + obj.getPoints();
				}
				loyaltyObj.setVersion(loyaltyObj.getVersion() + 1L);
				LoyaltyPoints points = new LoyaltyPoints();
				points.setBalance(new LoyaltyPointsBalance()
						.setString(poin.toString() + " of " + obj.getTotalPoints().toString()));
				points.setLabel("STAMPS");
				loyaltyObj.setLoyaltyPoints(points);
				
				
				ObjectData classData = getObjectData(KeyFactory.stringToKey(classKey));
				ObjectData objData = getObjectData(KeyFactory.stringToKey(obj.getObjectKey()));
				Entity got = datastore.get(KeyFactory.stringToKey(classKey));
				String rewardStatus = (String) got.getProperty("rewardStatus");
				Entity objectData = datastore.get(KeyFactory.stringToKey(obj.getObjectKey()));
				objectData.setProperty("rewardStatus", rewardStatus);
				datastore.put(objectData);
				
				String updateResponse = updatePoint(obj.getObjectKey(), poin, obj.getTotalPoints());
				if (!(updateResponse.equalsIgnoreCase(GeneralConstants.MESSAGE_SUCCESS))) {
					throw new Exception(updateResponse);
				}
				
				availableRewardList = getAvailableReward(classData.getRewardStatus(), poin, obj.getTotalPoints());
				List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
				WalletObjectMessage message = new WalletObjectMessage();
				message.setBody("You have " + String.valueOf(poin) + " of " + obj.getTotalPoints() + " stamps" + "\r\n"
						+ "\r\n" + availableRewardList);
				message.setHeader(loyaltyObj.getMessages().get(0).getHeader());
				messages.add(message);
				loyaltyObj.setMessages(messages);
				stampImageUrl = ImageStamp.createStampImage(poin, obj.getTotalPoints(), obj.getObjectId(),
						backgroundImage, stampImage, classData.getRewardStatus(), objData.getLastClaimed(),
						objData.getLastClaimedProduct());
				List<ImageModuleData> imageModuleData = new ArrayList<ImageModuleData>();
				ImageModuleData image = new ImageModuleData()
						.setMainImage(new Image().setSourceUri(new Uri().setUri(stampImageUrl)));
				imageModuleData.add(image);
				loyaltyObj.setImageModulesData(imageModuleData);
				try {
					response = client.loyaltyobject().update(issuerId + "." + obj.getObjectId(), loyaltyObj).execute();
				} catch (Exception e) {
					resp2 = new KittiResponse(null, "Failed, " + e.getMessage(), true);
				}
				loyaltyObjectResponse.setCurrentPoints(poin);
				loyaltyObjectResponse.setObjectId(obj.getObjectId());
				loyaltyResp.add(loyaltyObjectResponse);
				resp2.setResponse(loyaltyResp);
			} else {
				int points = obj.getPoints();
				Double totalCard = (double) points / obj.getTotalPoints();
				Integer totalCardCeil = (int) Math.ceil(totalCard);
				int newPoints;
				if (points > obj.getTotalPoints()) {
					newPoints = obj.getTotalPoints();
				} else {
					newPoints = obj.getPoints();
				}
				int pointAdded = 0;
				for (int i = 0; i < totalCardCeil; i++) {
					rand = generator.nextInt(900000) + 100000;
					String objId = obj.getClassId() + userId + rand, objectKey = null;
					try {
						objectKey = createDatabaseId(classKey, objId, newPoints, obj.getTotalPoints(), backgroundImage,
								stampImage, obj.getClassId());
					} catch (Exception e) {
						e.printStackTrace();
						resp2 = new KittiResponse(null, "Failed, " + e.getMessage(), true);
					}
					Key objectKeys = KeyFactory.stringToKey(objectKey);
					ObjectData objData = getObjectData(objectKeys);
					availableRewardList = getAvailableReward(objData.getRewardStatus(), newPoints,
							obj.getTotalPoints());
					stampImageUrl = ImageStamp.createStampImage(newPoints, obj.getTotalPoints(), objId, backgroundImage,
							stampImage, objData.getRewardStatus(), objData.getLastClaimed(),
							objData.getLastClaimedProduct());
					if (stampImageUrl == "") {
						throw new Exception("Failed read image");
					}
					payload.addObject(Loyalty.generateLoyaltyObject(utils2.getIssuerId(), obj.getUserName(),
							obj.getClassId(), objId, obj.getUserId(), newPoints, obj.getTotalPoints(), stampImageUrl,
							availableRewardList));
					loyaltyObjectResponse = new LoyaltyObjectResponse();
					loyaltyObjectResponse.setObjectId(objId);
					loyaltyObjectResponse.setObjectKey(objectKey);
					try {
						jwt = utils2.generateSaveJwt(payload, origins);
					} catch (SignatureException e) {
						e.printStackTrace();
						resp2 = new KittiResponse(null, "Failed, " + e.getMessage(), true);
					}
					link = LINK_SAVE_CARD + jwt;
					loyaltyObjectResponse.setCurrentPoints(newPoints);
					loyaltyObjectResponse.setLinkLoyalty(link);
					loyaltyResp.add(loyaltyObjectResponse);
					pointAdded += newPoints;
					if (points - pointAdded > obj.getTotalPoints()) {
						newPoints = obj.getTotalPoints();
					} else {
						newPoints = points - pointAdded;
					}
					jwt = null;
					link = null;
					payload = new WobPayload();
				}
				resp2.setResponse(loyaltyResp);
			}
			resp2 = new KittiResponse(loyaltyResp, GeneralConstants.MESSAGE_SUCCESS, false);
		} catch (Exception e) {
			resp2 = new KittiResponse(null, "Failed, " + e.getMessage(), true);
		}
		String respon = new Gson().toJson(resp2, KittiResponse.class);
		PrintWriter out = resp.getWriter();
		out.write(respon);
	}

	public static String getAvailableReward(List<RewardStatus> rwdStat, int currentPoints, int totalPoints) {
		String listReward = null, rwdlist = "Here are the option of rewards that you will get : " + "\r\n";
		int firstMilestone = rwdStat.get(0).getMilestone();int countStatusClaimed = 0;
		try {
			String[] allRewards = new String[] {};
			listReward = "";
			List<String> listrwd = new ArrayList<String>();
			
			for (int c = 0; c < rwdStat.size(); c++) {
				String[] currentReward = rwdStat.get(c).getRewards();
				int currentMilestone = rwdStat.get(c).getMilestone();
				String statusClaimed = rwdStat.get(c).getStatus();
				if (statusClaimed.equalsIgnoreCase(GeneralConstants.STATUS_CLAIMED)) {
					countStatusClaimed++;
					break;
				} else if (currentPoints >= currentMilestone
						&& statusClaimed.equalsIgnoreCase(GeneralConstants.STATUS_UNCLAIMED)) {
					allRewards = ArrayUtils.addAll(allRewards, currentReward);
				} else if (currentPoints < currentMilestone
						&& statusClaimed.equalsIgnoreCase(GeneralConstants.STATUS_UNCLAIMED)) {
					rwdlist += currentMilestone + " stamps : " + Arrays.toString(currentReward).replace("[", "").replace("]", "") + "\r\n";
				}
			}
			if (countStatusClaimed != 0) {
				listReward = "You have claimed the reward !!";
			} else {
				listrwd = Arrays.asList(allRewards);
				listReward = getListAvalaibleReward(listrwd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (countStatusClaimed > 0) {
			return listReward = "You have claimed the reward !!";
		}else if (currentPoints < firstMilestone) {
			return rwdlist;
		} else if (currentPoints == totalPoints) {
			return listReward;
		} else {
			return listReward + "\r\n" + rwdlist;
		}
	}

	/**
	 * @param listrwd
	 * @return
	 */
	private static String getListAvalaibleReward(List<String> listrwd) {
		String listReward;
		String availableReward = listrwd.toString().replace("]", "").replace("[", "");
		String[] availableRewardString = availableReward.split(", ");
		HashMap<String, Integer> map = new HashMap<>();
		int count = 1;
		listReward = "You can only claim one of the following items : " + "\r\n";
		for (String x : availableRewardString) {
			if (map.containsKey(x)) {
				int value = map.get(x);
				int newValue = value + 1;
				map.replace(x, newValue);
				listReward = listReward.replace(value + " " + x + "\r\n", newValue + " " + x + "\r\n");
			} else {
				map.put(x, count);
				listReward += count + " " + x + "\r\n";
			}
		}
		return listReward;
	}

	private static ObjectData getObjectData(Key key) {
		ObjectData objData = new ObjectData();
		try {
			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Entity got = datastore.get(key);
			String rewardStatus = (String) got.getProperty("rewardStatus");

			String jsonString = "{\"RewardStatus\":" + rewardStatus + "}";
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray js = jsonObject.getJSONArray("RewardStatus");
			List<RewardStatus> rwdStat = new ArrayList<RewardStatus>();

			for (int j = 0; j < js.length(); j++) {
				JSONObject objects = js.getJSONObject(j);
				JSONArray arrayReward = objects.getJSONArray("rewards");
				List<String> listrwd = new ArrayList<String>();
				for (int k = 0; k < arrayReward.length(); k++) {
					listrwd.add(arrayReward.getString(k));
				}
				String[] stringArray = listrwd.toArray(new String[listrwd.size()]);
				RewardStatus rwstat = new RewardStatus();
				rwstat.setMilestone(objects.getInt("milestone"));
				rwstat.setStatus(objects.getString("status"));
				rwstat.setRewards(stringArray);
				rwdStat.add(rwstat);
			}
			Long lastClaimed = (Long) got.getProperty("lastClaimed");
			String lastClaimedProduct = (String) got.getProperty("lastClaimedProduct");
			objData.setLastClaimed(String.valueOf(lastClaimed));
			objData.setLastClaimedProduct(lastClaimedProduct);
			objData.setRewardStatus(rwdStat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objData;
	}

	private static String createDatabaseId(String classKey, String objectId, int points, int totalPoints,
			String backgroundImage, String stampImage, String classId) {
		Key classKeys = KeyFactory.stringToKey(classKey);
		Entity got = null;
		String objectKey = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		try {
			got = datastore.get(classKeys);
			String rewardStatus = (String) got.getProperty("rewardStatus");
			Entity objectDatabase = new Entity(GeneralConstants.LOYALTY_OBJECT_ENTITY, objectId);
			objectDatabase.setProperty("rewardStatus", rewardStatus);
			objectDatabase.setProperty("lastClaimed", null);
			objectDatabase.setProperty("lastClaimedProduct", null);
			objectDatabase.setProperty("points", points);
			objectDatabase.setProperty("totalPoints", totalPoints);
			objectDatabase.setProperty("backgroundImage", backgroundImage);
			objectDatabase.setProperty("stampImage", stampImage);
			objectDatabase.setProperty("classId", classId);
			objectDatabase.setProperty("objectId", objectId);
			datastore.put(objectDatabase);
			Key loyaltyObjectKey = objectDatabase.getKey();
			objectKey = KeyFactory.keyToString(loyaltyObjectKey);
			Entity gotNew = datastore.get(loyaltyObjectKey);
			gotNew.setProperty("objectKey", objectKey);
			datastore.put(gotNew);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objectKey;
	}

	private static String updatePoint(String objectKey, int point, int totalPoint) {
		Key objectKeys = KeyFactory.stringToKey(objectKey);
		Entity got = null;
		String response = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		try {
			got = datastore.get(objectKeys);
			got.setProperty("points", point);
			got.setProperty("totalPoints", totalPoint);
			datastore.put(got);
			response = GeneralConstants.MESSAGE_SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			response = e.getMessage();
		}
		return response;
	}
}
