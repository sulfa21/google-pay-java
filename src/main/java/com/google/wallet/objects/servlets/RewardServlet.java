package com.google.wallet.objects.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.google.api.client.json.GenericJson;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.Image;
import com.google.api.services.walletobjects.model.ImageModuleData;
import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.api.services.walletobjects.model.LoyaltyPoints;
import com.google.api.services.walletobjects.model.Uri;
import com.google.api.services.walletobjects.model.WalletObjectMessage;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.AccessRewardResponse;
import com.google.wallet.objects.models.ClaimRewardRequest;
import com.google.wallet.objects.models.ClaimRewardResponse;
import com.google.wallet.objects.utils.ImageStamp;
import com.google.wallet.objects.utils.RewardStatus;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobUtils;

/**
 * @author sulfa
 *
 */
public class RewardServlet extends HttpServlet {
	private static Gson gson = new Gson();
	private final long serialVersionUID = 1L;
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ClaimRewardResponse lcresp = new ClaimRewardResponse();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity gotNew = null;
		List<RewardStatus> rwdStatNew = null, rwdStat = null;
		String stampImageUrl = null;
		JwtServlet jwtServlet = new JwtServlet();
		try {
			ClaimRewardRequest obj = new Gson().fromJson(req.getReader(), ClaimRewardRequest.class);
			if(obj.getProductClaimed() == null || obj.getRewardClaimed()<1) {
				throw new Exception("Claimed product or Claimed Milestone can't be null");
			}
			Key objKey = KeyFactory.stringToKey(obj.getObjectKey());
			AccessRewardResponse updateDatastore =updateDatastore(objKey, obj.getRewardClaimed(), obj.getProductClaimed());
			if(!(updateDatastore.getResponseMessage()).equalsIgnoreCase(GeneralConstants.MESSAGE_SUCCESS)) {
				throw new Exception(updateDatastore.getResponseMessage());
			}
			rwdStat = updateDatastore.getRewardStat();
			String updateLoyaltyObject = updateLoyaltyObject(objKey, obj.getObjectId(), rwdStat, obj.getProductClaimed());
			if(updateLoyaltyObject.equalsIgnoreCase(GeneralConstants.MESSAGE_SUCCESS)) {
				lcresp = new ClaimRewardResponse(GeneralConstants.MESSAGE_SUCCESS, false);
			}else {
				lcresp = new ClaimRewardResponse(updateLoyaltyObject, true);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			lcresp.setResponseMessage(e.getMessage());
			lcresp.setIsError(true);
		}
		PrintWriter out = resp.getWriter();
		out.write(gson.toJson(lcresp));
	}

	public static List<RewardStatus> getRewardList(String reward, int rewardClaimed, String productClaimed)
			throws JSONException {
		List<RewardStatus> rwdStat = new ArrayList<RewardStatus>();
		String jsonString = "{\"RewardStatus\":" + reward + "}";
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray js = jsonObject.getJSONArray("RewardStatus");

		try {
			for (int j = 0; j < js.length(); j++) {
				JSONObject objects = js.getJSONObject(j);

				JSONArray arrayReward = objects.getJSONArray("rewards");
				List<String> listrwd = new ArrayList<String>();
				for (int k = 0; k < arrayReward.length(); k++) {
					listrwd.add(arrayReward.getString(k));
				}
				String[] stringArray = listrwd.toArray(new String[listrwd.size()]);
				List<String> list = new ArrayList<String>(Arrays.asList(stringArray));
				if (rewardClaimed != 0 && productClaimed != null) {
					if (objects.getInt("milestone") == rewardClaimed) {
						list.remove(productClaimed);
						stringArray = list.toArray(new String[list.size()]);
					}
				}
				RewardStatus rwstat = new RewardStatus();
				rwstat.setMilestone(objects.getInt("milestone"));
				rwstat.setStatus(objects.getString("status"));
				rwstat.setRewards(stringArray);
				rwdStat.add(rwstat);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rwdStat;

	}

	private static JSONArray getRewardArray(List<RewardStatus> rwdStat, int rewardClaimed) {
		JSONArray rewardStatusArray = new JSONArray();
		HashMap<String, JSONObject> maprewards = new HashMap<String, JSONObject>();
		String statusMilestone = null;
		try {
			for (int k = 0; k < rwdStat.size(); k++) {
				JSONObject jsonReward = new JSONObject();
				if (rwdStat.get(k).getMilestone() == rewardClaimed) {
					statusMilestone = GeneralConstants.STATUS_CLAIMED;
				} else {
					statusMilestone = rwdStat.get(k).getStatus();
				}
				ArrayList<String> listRewards = new ArrayList<String>();
				String[] lr = rwdStat.get(k).getRewards();
				for (String x : lr) {
					listRewards.add(x);
				}
				jsonReward.put("milestone", rwdStat.get(k).getMilestone());
				jsonReward.put("status", statusMilestone);
				jsonReward.put("rewards", new JSONArray(listRewards));
				maprewards.put("jsonReward" + k, jsonReward);
				rewardStatusArray.put(maprewards.get("jsonReward" + k));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rewardStatusArray;
	}

	
	
	private static AccessRewardResponse updateDatastore(Key objKey, int rewardClaimed, String productClaimed) {
		Entity got = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		AccessRewardResponse response = new AccessRewardResponse();
		try {
			got = datastore.get(objKey);
			String rewardStatus = (String) got.getProperty("rewardStatus");
			List<RewardStatus> rwdStat = getRewardList(rewardStatus, rewardClaimed, productClaimed);
			JSONArray rewardStatusArray = getRewardArray(rwdStat, rewardClaimed);
			got.setProperty("lastClaimed", rewardClaimed);
			got.setProperty("lastClaimedProduct", productClaimed);
			got.setProperty("rewardStatus", rewardStatusArray.toString());
			datastore.put(got);
			response = new AccessRewardResponse(rwdStat, GeneralConstants.MESSAGE_SUCCESS, false);
		}catch(Exception e) {
			e.printStackTrace();
			response = new AccessRewardResponse(null, "Failed, " + e.getMessage(), true);
		
		}
		return response;
	}
	
	private static String  updateLoyaltyObject(Key objKey, String objectId, List<RewardStatus> rwdStat, String productClaimed) {
		String responseMessage  = null;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity gotNew = null;
		List<RewardStatus> rwdStatNew = null;
		String stampImageUrl = null;
		JwtServlet jwtServlet = new JwtServlet();
		try {
			String issuerId = GeneralConstants.ISSUER_ID;
			WobCredentials credentials = new WobCredentials(GeneralConstants.SERVICE_ACCOUNT_ID,
					GeneralConstants.SERVICE_ACCOUNT_PRIVATE_KEY, GeneralConstants.APPLICATION_NAME, issuerId);
			WobUtils utils2 = null;
			Walletobjects client = null;
			GenericJson response = null;

			try {
				client = new WobUtils(credentials).getClient();
			} catch (Exception e) {
				responseMessage = "Failed, " + e.getMessage();
			}
			LoyaltyObject loyaltyObj = client.loyaltyobject().get(issuerId + "." + objectId).execute();
			if(loyaltyObj.getState().equalsIgnoreCase(GeneralConstants.STATE_INACTIVE)) throw new Exception("Failed claim reward, This pass has expired");
			loyaltyObj.setVersion(loyaltyObj.getVersion() + 1L);
			gotNew = datastore.get(objKey);
			String rewardStatusNew = (String) gotNew.getProperty("rewardStatus");
			rwdStatNew = getRewardList(rewardStatusNew, 0, null);
			
			Long point = (Long) gotNew.getProperty("points");
			Long totalPoint = (Long) gotNew.getProperty("totalPoints");	
			String backgroundImage = (String) gotNew.getProperty("backgroundImage");
			String stampImage = (String) gotNew.getProperty("stampImage");
			Long lastClaimedNew = (Long) gotNew.getProperty("lastClaimed");
			String availableRewardList = JwtServlet.getAvailableReward(rwdStatNew, point.intValue(), totalPoint.intValue());
			
			List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
			WalletObjectMessage message = new WalletObjectMessage();
			message.setBody("You have " + String.valueOf(point.intValue()) + " of " + totalPoint.intValue() + " stamps" + "\r\n" + "\r\n" +  availableRewardList);
			message.setHeader(loyaltyObj.getMessages().get(0).getHeader());
			messages.add(message);
			loyaltyObj.setMessages(messages);
			
			stampImageUrl = ImageStamp.createStampImage(point.intValue(), totalPoint.intValue(), objectId,
					backgroundImage, stampImage, rwdStatNew, String.valueOf(lastClaimedNew), productClaimed);
			if(stampImageUrl== "") {
				throw new Exception("Failed get Image URL");
			}
			
			List<ImageModuleData> imageModuleData = new ArrayList<ImageModuleData>();
			ImageModuleData image = new ImageModuleData()
					.setMainImage(new Image().setSourceUri(new Uri().setUri(stampImageUrl)));
			imageModuleData.add(image);
			loyaltyObj.setImageModulesData(imageModuleData);
			loyaltyObj.setState(GeneralConstants.STATE_INACTIVE);
			
			try {
				response = client.loyaltyobject().update(issuerId + "." + objectId, loyaltyObj).execute();
				responseMessage = GeneralConstants.MESSAGE_SUCCESS;
			}catch(Exception e) {
				responseMessage = "Failed, " + e.getMessage();
			}
		}catch(Exception e) {
			responseMessage = "Failed, " + e.getMessage();
		}	
		return responseMessage;
	}

}
