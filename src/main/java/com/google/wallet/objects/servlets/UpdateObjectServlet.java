package com.google.wallet.objects.servlets;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.security.KeyStoreException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.google.api.client.json.GenericJson;
import com.google.api.services.walletobjects.Walletobjects;
import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.api.services.walletobjects.model.WalletObjectMessage;
import com.google.gson.Gson;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.CreateLoyaltyClassRequest;
import com.google.wallet.objects.models.UpdateLoyaltyClassResponse;
import com.google.wallet.objects.models.UpdateLoyaltyObjectRequest;
import com.google.wallet.objects.utils.WobCredentials;
import com.google.wallet.objects.utils.WobPayload;
import com.google.wallet.objects.utils.WobUtils;

/**
 * @author sulfa
 *
 */
public class UpdateObjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Gson gson = new Gson();

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		UpdateLoyaltyClassResponse lcresp = new UpdateLoyaltyClassResponse();
		GenericJson response = null;
		try {
			String issuerId = GeneralConstants.ISSUER_ID;
			UpdateLoyaltyObjectRequest obj = new Gson().fromJson(req.getReader(), UpdateLoyaltyObjectRequest.class);
			Walletobjects client = null;
			WobCredentials credentials = new WobCredentials(GeneralConstants.SERVICE_ACCOUNT_ID,
					GeneralConstants.SERVICE_ACCOUNT_PRIVATE_KEY, GeneralConstants.APPLICATION_NAME, issuerId);
			try {
				client = new WobUtils(credentials).getClient();
			} catch (Exception e) {
				lcresp = new UpdateLoyaltyClassResponse("Failed, " + e.getMessage(), true);
			}
			LoyaltyObject loyaltyObj = client.loyaltyobject().get(issuerId + "." + obj.getObjectId()).execute();
			if (loyaltyObj.size() < 0) {
				throw new Exception("Object Id not found");
			}
			loyaltyObj.setVersion(loyaltyObj.getVersion() + 1L);

			if (obj.getStatus().equalsIgnoreCase(GeneralConstants.STATE_ACTIVE)) {
				loyaltyObj.setState(GeneralConstants.STATE_ACTIVE);
			} else {
				loyaltyObj.setState(GeneralConstants.STATE_INACTIVE);
				List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
				WalletObjectMessage message = new WalletObjectMessage().setBody("This pass has expired");
				messages.add(message);
				loyaltyObj.setMessages(messages);
				
			}
			response = client.loyaltyobject().update(issuerId + "." + obj.getObjectId(), loyaltyObj).execute();
			lcresp = new UpdateLoyaltyClassResponse(GeneralConstants.MESSAGE_SUCCESS, false);
		} catch (Exception e) {
			lcresp = new UpdateLoyaltyClassResponse("Failed, " + e.getMessage(), false);
		}
		String respon = new Gson().toJson(lcresp, UpdateLoyaltyClassResponse.class);
		PrintWriter out = resp.getWriter();
		out.write(respon);
	}
}
