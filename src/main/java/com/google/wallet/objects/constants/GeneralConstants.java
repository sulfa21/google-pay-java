package com.google.wallet.objects.constants;

/**
 * @author sulfa
 * Development 
 * =======================================
 * --ISSUER ID : 3372601133638565184
 * --ISSUER NAME : Geekseat
 * --LOYALTY_CLASS_ENTITY : LoyaltyClass
 * --LOYALTY_OBJECT_ENTITY : LoyaltyObject
 * --BUCKET_NAME : image_bucket-123
 * 
 * 
 * Production
 * =======================================
 * 
 * --ISSUER ID : 3378336392711758501
 * --ISSUER NAME : Kitti Loyalty Pty Ltd
 * --LOYALTY_CLASS_ENTITY : LoyaltyClassProd
 * --LOYALTY_OBJECT_ENTITY : LoyaltyObjectProd
 * --BUCKET_NAME : image_bucket-123-prod
 */
public class GeneralConstants {
	public static final String SERVICE_ACCOUNT_ID = "loyaltyproject@loyaltyproject-256603.iam.gserviceaccount.com";
	public static final String SERVICE_ACCOUNT_PRIVATE_KEY = "WEB-INF/loyaltyproject-256603-d65dc1779981.p12";
	public static final String APPLICATION_NAME ="My First Test";
	public static final String ISSUER_ID ="3378336392711758501";
	public static final String APPLE_PRIVATE_KEY = "WEB-INF/Certificates.p12";
	public static final String APPLE_CERTIFICATE = "WEB-INF/AppleWWDRCA.cer";
	public static final String STATUS_CLAIMED = "claimed";
	public static final String STATUS_UNCLAIMED = "unclaimed";
	public static final String MESSAGE_SUCCESS = "Success";
	public static final String STATE_ACTIVE  = "active";
	public static final String STATE_INACTIVE = "inactive";
	public static final String DEFAULT_HEADER_MESSAGE = "Desccription";
	public static final String DEFAULT_PROGRAM_NAME = "Loyalty Program";
	public static final String DEFAULT_FONT_COLOR = "#ABABAB";
	public static final String DEFAULT_BODY_MESSAGE = "Welcome !!";
	public static final String DEFAULT_BACKGROUND_COLOR = "#FF3300";
	public static final String DEFAULT_URI_LOGO_IMAGE = "https://hariannusantara.com/wp-content/uploads/2019/05/Gambar-Kartun-Kucing-Lucu4.jpg";
	public static final String BUCKET_NAME = "image_bucket-123-prod";
	public static final String LOYALTY_CLASS_ENTITY = "LoyaltyClassProd";
	public static final String LOYALTY_OBJECT_ENTITY = "LoyaltyObjectProd";
}
