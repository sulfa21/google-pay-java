package com.google.wallet.objects.verticals;

import java.util.ArrayList;
import java.util.List;
import com.google.api.services.walletobjects.model.Barcode;
import com.google.api.services.walletobjects.model.DateTime;
import com.google.api.services.walletobjects.model.Image;
import com.google.api.services.walletobjects.model.ImageModuleData;
import com.google.api.services.walletobjects.model.LatLongPoint;
import com.google.api.services.walletobjects.model.LoyaltyClass;
import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.api.services.walletobjects.model.LoyaltyPoints;
import com.google.api.services.walletobjects.model.LoyaltyPointsBalance;
import com.google.api.services.walletobjects.model.TimeInterval;
import com.google.api.services.walletobjects.model.TypedValue;
import com.google.api.services.walletobjects.model.Uri;
import com.google.api.services.walletobjects.model.WalletObjectMessage;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.CreateLoyaltyClassRequest;
import com.google.wallet.objects.utils.Location;

/**
 * Class to generate example Loyalty class and objects
 *
 * @author sulfa
 *
 */
public class Loyalty {
	/**
	 * Generates a Loyalty Object
	 *
	 * @param issuerId
	 * @param classId
	 * @param objectId
	 * @return loyaltyObject
	 */
	public static LoyaltyObject generateLoyaltyObject(String issuerId, String username, String classId, String objectId,
			String userId, Integer earnPoints, Integer totalPoints, String stampImageUrl, String availableRewardList) {

		// Define Barcode
		Barcode barcode = new Barcode().setType("qrCode").setValue(userId).setAlternateText(userId);

		// Define Messages:
		List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
		WalletObjectMessage message = new WalletObjectMessage().setHeader("Hello " + username)
				.setBody("You have " + String.valueOf(earnPoints) + " of " + totalPoints + " stamps" + "\r\n" + "\r\n"
						+ availableRewardList);
		messages.add(message);

		// Define Points
		LoyaltyPoints points = new LoyaltyPoints().setLabel("Stamps")
				.setBalance(new LoyaltyPointsBalance().setString(earnPoints.toString() + " of " + totalPoints))
				.setPointsType("rewards");

		List<ImageModuleData> imageModuleData = new ArrayList<ImageModuleData>();
		ImageModuleData image = new ImageModuleData()
				.setMainImage(new Image().setSourceUri(new Uri().setUri(stampImageUrl)));
		imageModuleData.add(image);

		// Define Wallet Instance
		LoyaltyObject object = new LoyaltyObject().setClassId(issuerId + "." + classId).setId(issuerId + "." + objectId)
				.setVersion(1L).setState("active").setBarcode(barcode).setAccountName(username).setAccountId(userId)
				.setLoyaltyPoints(points).setMessages(messages).setImageModulesData(imageModuleData);
		return object;
	}

	/**
	 * Generates a Loyalty Class
	 *
	 * @param issuerId
	 * @param classId
	 * @return loyaltyClass
	 */
	public static LoyaltyClass generateLoyaltyClass(CreateLoyaltyClassRequest req) {

		// Define general messages
		List<WalletObjectMessage> messages = new ArrayList<WalletObjectMessage>();
		WalletObjectMessage message = new WalletObjectMessage().setHeader(req.getHeaderMessage())
				.setBody(req.getBodyMessage())
				.setImage(new Image().setSourceUri(
						new Uri().setUri("http://farm4.staticflickr.com/3723/11177041115_6e6a3b6f49_o.jpg")))
				.setActionUri(new Uri().setUri("http://baconrista.com"));

		messages.add(message);

		// Define Class IssuerData
		TypedValue issuerData = new TypedValue();
		TypedValue gExpanded = new TypedValue();
		TypedValue textModule = new TypedValue();
		textModule.put("header", new TypedValue().setString("Rewards details"));
		textModule.put("body", new TypedValue().setString(req.getBodyMessage()));
		TypedValue linksModule = new TypedValue();

		if (req.getUriHomepage() != null) {
			linksModule.put("uri0",
					new TypedValue().setUri(new Uri().setUri(req.getUriHomepage()).setDescription("Rewards")));
			gExpanded.put("linksModule", linksModule);
		}
		TypedValue infoModule = new TypedValue();

		infoModule.put("fontColor", new TypedValue().setString(req.getFontColor()));
		infoModule.put("backgroundColor", new TypedValue().setString(req.getBackgroundColor()));
		infoModule.put("row0",
				new TypedValue()
						.set("col0",
								new TypedValue().set("label", new TypedValue().setString("Label 0")).set("value",
										new TypedValue().setString("Value 0")))
						.set("col1", new TypedValue().set("label", new TypedValue().setString("Label 1")).set("value",
								new TypedValue().setString("Value1"))));
		infoModule.put("row1",
				new TypedValue()
						.set("col0",
								new TypedValue().set("label", new TypedValue().setString("Label 0")).set("value",
										new TypedValue().setString("Value 0")))
						.set("col1", new TypedValue().set("label", new TypedValue().setString("Label 1")).set("value",
								new TypedValue().setString("Value1"))));
		gExpanded.put("textModule", textModule);
		gExpanded.put("infoModule", infoModule);
		issuerData.put("g_expanded", gExpanded);

		List<LatLongPoint> locations = new ArrayList<LatLongPoint>();
		LatLongPoint llp = new LatLongPoint();
		// Define Geofence locations
		if (req.getLocation() != null) {
			for (Location loc : req.getLocation()) {
				llp = new LatLongPoint();
				llp.setLatitude(loc.getLatitude());
				llp.setLongitude(loc.getLongitude());
				locations.add(llp);
			}
		}

		LoyaltyClass wobClass = new LoyaltyClass();
		wobClass.setId(GeneralConstants.ISSUER_ID + "." + req.getClassId());
		wobClass.setProgramName(req.getProgramName());
		wobClass.setVersion(1L);
		wobClass.setIssuerName(req.getIssuerName());
		if (req.getUriHomepage() != null) {
			wobClass.setHomepageUri(new Uri().setUri(req.getUriHomepage()));
		}
		wobClass.setProgramLogo(new Image().setSourceUri(new Uri().setUri(req.getUriLogoImage())));
		wobClass.setAccountIdLabel("Member Id");
		wobClass.setAccountNameLabel("Member Name");
		wobClass.setMessages(messages);
		wobClass.setReviewStatus("underReview");
		wobClass.setAllowMultipleUsersPerObject(true);
		if (locations != null) {
			wobClass.setLocations(locations);
		}
		wobClass.setIssuerData(issuerData);
		return wobClass;
	}
}
