package com.google.wallet.objects.utils;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.wallet.objects.constants.GeneralConstants;
import com.google.wallet.objects.models.LeftRewards;

/**
 * @author sulfa
 *
 */
public class ImageStamp {
	
	/**
	 * 
	 */
	private static final int IMAGE_HEIGHT = 246;
	/**
	 * 
	 */
	private static final int IMAGE_WIDTH = 620;
	private static final int HEIGHT_BACKGROUND_IMAGE = 246;
	private static final int WIDTH_BACKGROUND_IMG = 620;
	private final static String bucket = GeneralConstants.BUCKET_NAME;
	private static final int MAX_DISTANCE_NOTIFICATION = 100; //in meters
	private static final String FONT_TYPE = "Roboto";
	private static final Random generator = new Random();
	private final static GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
			.initialRetryDelayMillis(10).retryMaxAttempts(10).totalRetryPeriodMillis(15000).build());

	public static byte[] recoverImageFromUrl(String urlText) throws Exception {
		URL url = new URL(urlText);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try (InputStream inputStream = url.openStream()) {
			int n = 0;
			byte[] buffer = new byte[1024];
			while (-1 != (n = inputStream.read(buffer))) {
				output.write(buffer, 0, n);
			}
		}
		return output.toByteArray();
	}

	
	public static TemplateStamp templateStampSize(int totalPoint) {
		TemplateStamp templateStamp = new TemplateStamp();
		try {
			switch (totalPoint) {
			case 1:
				templateStamp = new TemplateStamp(240,57, 184, 1, 137); 
				break;
			case 2:
				templateStamp = new TemplateStamp(139, 57, 213, 2, 127); 
				break;
			case 3:
				templateStamp = new TemplateStamp(50, 60, 195, 3, 128); 
				break;
			case 4:
				templateStamp = new TemplateStamp(42, 71, 145, 4, 103); 
				break;
			case 5:
				templateStamp = new TemplateStamp(46, 80, 111, 5, 82);  
				break;
			case 6:
				templateStamp = new TemplateStamp(30, 90, 98, 6, 68); 
				break;
			case 7:
				templateStamp = new TemplateStamp(88, 133, 35, 121, 4, 80); 
				break;
			case 8:
				templateStamp = new TemplateStamp(74, 132, 32, 130, 4, 80); 
				break;
			case 9:
				templateStamp = new TemplateStamp(33, 138, 22, 117, 5, 85); 
				break;
			case 10:
				templateStamp = new TemplateStamp(33, 138, 22, 117, 5, 85); 
				break;
			case 11:
				templateStamp = new TemplateStamp(30, 135, 43, 98, 6, 68); 
				break;
			case 12:
				templateStamp = new TemplateStamp(30, 135, 43, 98, 6, 68); 
				break;
			case 13:
				templateStamp = new TemplateStamp(41, 133, 42, 78, 7, 68); 
				break;
			case 14:
				templateStamp = new TemplateStamp(41, 133, 42, 78, 7, 68); 
				break;
			case 15:
				templateStamp = new TemplateStamp(36, 140, 50, 70, 8, 58); 
				break;
			case 16:
				templateStamp = new TemplateStamp(36, 140, 49, 70, 8, 58);
				break;
			case 17:
				templateStamp = new TemplateStamp(12, 136, 52, 67, 9, 57); 
				break;
			case 18:
				templateStamp = new TemplateStamp(12, 138, 52, 67, 9, 57); 
				break;
			case 19:
				templateStamp = new TemplateStamp(28, 145, 51, 57, 10, 51);
				break;
			case 20:
				templateStamp = new TemplateStamp(28, 145, 51, 57, 10, 51); 
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return templateStamp;
	}

	public static BufferedImage resize(final URL url, int width, int height) throws IOException {
		final BufferedImage image = ImageIO.read(url);
		final BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D g = resized.createGraphics();
		g.drawImage(image, 0, 0, width, height, null);
		g.dispose();
		return resized;
	}

	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp =  img.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage((java.awt.Image) tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	private static String drawTextImage(File plainBackground, BufferedImage bgResized, String leftRewards, String objectId)
			throws IOException {
		BufferedImage imageTemplateRead = ImageIO.read(plainBackground);
		String url = "";
		Graphics2D g = imageTemplateRead.createGraphics();
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
		g.drawImage(bgResized, 0, 0, null);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f));
		//g.setFont(new Font(FONT_TYPE, Font.CENTER_BASELINE, 25));
		//g.drawString(1 + " FREE " + leftRewards.toUpperCase(), 50, 100);
		//g.drawString("HAS BEEN CLAIMED", 50, 130);
		String space ="%20", text = "1"+space+"FREE"+space+leftRewards.toUpperCase()+space+"HAVE"+space+"BEEN"+space+"CLAIMED";
		String texturl = "http://chart.apis.google.com/chart?chs=620x40&cht=p3&chtt=" + text
				+ "&chts=FFFFFF,25&chf=bg,s,000000";
		BufferedImage imagetext =  resize(new URL(texturl), 620, 40);
		g.drawImage(imagetext,0,90,null);
		g.dispose();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(imageTemplateRead, "png", output);
		byte [] data = output.toByteArray();
		int rand = 0;
		rand = generator.nextInt(900000) + 100000;
		url = getServingUrl(bucket, data, objectId + String.valueOf(rand) + ".jpeg");
		return url;
	}
	private static String drawStampImage(int points, TemplateStamp ts, BufferedImage backgroundImgResized,
			int upperXcoordinate, int upperYcoordinate, int stampSpace, int i, int maxStampPerRow, int stampSize,
			int yp, int currentmaxstamp, int currentRow, BufferedImage templateStampImgRead,
			BufferedImage stampImgResized, BufferedImage freeRewardResized, HashMap<Integer, String> hmap, String objectId)
			throws IOException {
		String url = "";
		Graphics2D g = templateStampImgRead.createGraphics();
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f));
		g.drawImage(backgroundImgResized, 0, 0, null);
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.9f));
		while (i < points) {
			Color transparents = new Color(0x00FFFFFF, true);
			int freeReward = 0;
			String statusMilestoneReward = null;
			if (hmap.containsKey(i + 1)) {
				freeReward = i;
				statusMilestoneReward = hmap.get(i + 1);
			}
			if (statusMilestoneReward != null) {
				if (statusMilestoneReward.equalsIgnoreCase(GeneralConstants.STATUS_UNCLAIMED)) {
					g.setClip(new Ellipse2D.Float(upperXcoordinate, upperYcoordinate, stampSize, stampSize));
					g.drawImage(freeRewardResized, upperXcoordinate, upperYcoordinate, null);
					g.setPaint(transparents);
			        g.setStroke(new BasicStroke(1));
			        g.drawOval(upperXcoordinate, upperYcoordinate, stampSize, stampSize); 
				}
			} else {
				g.setClip(new Ellipse2D.Float(upperXcoordinate, upperYcoordinate, stampSize, stampSize));
				g.drawImage(stampImgResized, upperXcoordinate, upperYcoordinate, null);
				g.setPaint(transparents);
		        g.setStroke(new BasicStroke(1));
		        g.drawOval(upperXcoordinate, upperYcoordinate, stampSize, stampSize); 
			}
			i++;
			upperXcoordinate += stampSpace;
			if (i == currentmaxstamp) {
				upperYcoordinate += yp;
				upperXcoordinate = ts.getXa();
				currentRow++;
				currentmaxstamp = maxStampPerRow * currentRow;
			}
		}
		g.dispose();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(templateStampImgRead, "png", output);
		byte [] data = output.toByteArray();
		int rand = 0;
		rand = generator.nextInt(900000) + 100000;
		return url = getServingUrl(bucket, data, objectId + String.valueOf(rand) + ".jpeg");
	}

	public static com.google.wallet.objects.models.LeftRewards getLeftReward(List<RewardStatus> rewardStatusList, String lastClaimedProduct) {
		String leftRewards = "";
		int count = 0, countReward = 0;
		HashMap<Integer, String> hmap = new HashMap<Integer, String>();
		for (RewardStatus rs : rewardStatusList) {
			hmap.put(rs.getMilestone(), rs.getStatus());
			if (rs.getStatus().equalsIgnoreCase(GeneralConstants.STATUS_CLAIMED)) {
				count++;
			}
		}
		if (count != 0) {
			leftRewards = lastClaimedProduct.toUpperCase();
		}
		com.google.wallet.objects.models.LeftRewards leftRwd = new com.google.wallet.objects.models.LeftRewards(leftRewards, count, hmap, countReward);
		return leftRwd;
	}

	public static String createStampImage(int points, int totalPoints, String objectId, String backgroundImgUrl, String stampImgUrl,
			List<RewardStatus> rewardStatusList, String lastClaimed, String lastClaimedProduct)
			throws IOException {
				String url = "";
		try {
			String totalStampTemplate = String.valueOf(totalPoints);
			TemplateStamp ts = templateStampSize(totalPoints);

			File plainBackground = new File("WEB-INF/" + "plain.PNG");
			BufferedImage backgroundImgResized = ImageIO.read(plainBackground);
			if (backgroundImgUrl != null) {
				backgroundImgResized = resize(new URL(backgroundImgUrl), IMAGE_WIDTH, IMAGE_HEIGHT);
			}
			int upperXcoordinate = ts.getXa(), upperYcoordinate = ts.getYa(), lowerYcoordinate = ts.getYb(), stampSpace = ts.getXp(), i = 0,
					maxStampPerRow = ts.getMaxStampPerRow(), stampSize = ts.getStampSize(), yp = lowerYcoordinate - upperYcoordinate,
					currentmaxstamp = ts.getMaxStampPerRow(), currentRow = 1;
			File templateStampImage = new File("WEB-INF/" + totalStampTemplate + ".PNG"),
					freeRewardImage = new File("WEB-INF/"+ "rewards.PNG");
			BufferedImage templateStampImgRead = ImageIO.read(templateStampImage),
					templateStampImgResized = resize(templateStampImgRead, IMAGE_HEIGHT, IMAGE_WIDTH),
					stampImgResized = resize(new URL(stampImgUrl), stampSize, stampSize),
					freeRewardImgRead = ImageIO.read(freeRewardImage),
					freeRewardResized = resize(freeRewardImgRead, stampSize, stampSize);
			LeftRewards leftReward = getLeftReward(rewardStatusList, lastClaimedProduct);
			int claimedRewardCount = leftReward.getCount();
			HashMap<Integer, String> hmap = leftReward.getHmap();
			if (claimedRewardCount != 0) {
				url = drawTextImage(plainBackground, backgroundImgResized, lastClaimedProduct, objectId);
			} else {
				url = drawStampImage(points, ts, backgroundImgResized, upperXcoordinate, upperYcoordinate, stampSpace, i,
						maxStampPerRow, stampSize, yp, currentmaxstamp, currentRow, templateStampImgResized,
						stampImgResized, freeRewardResized, hmap, objectId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}

	

	private static List<RewardStatus> getRewardList(String reward) throws JSONException {
		List<RewardStatus> rwdStat = new ArrayList<RewardStatus>();
		String jsonString = "{\"RewardStatus\":" + reward + "}";
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray js = jsonObject.getJSONArray("RewardStatus");
		return rwdStat = getListRewardStatus(rwdStat, js);
	}

	private static List<RewardStatus> getListRewardStatus(List<RewardStatus> rwdStat, JSONArray js) throws JSONException {
		for (int j = 0; j < js.length(); j++) {
			JSONObject objects = js.getJSONObject(j);
			JSONArray arrayReward = objects.getJSONArray("rewards");
			List<String> listrwd = new ArrayList<String>();
			for (int k = 0; k < arrayReward.length(); k++) {
				listrwd.add(arrayReward.getString(k));
			}
			String[] stringArray = listrwd.toArray(new String[listrwd.size()]);
			RewardStatus rwstat = new RewardStatus();
			rwstat.setMilestone(objects.getInt("milestone"));
			rwstat.setStatus(objects.getString("status"));
			rwstat.setRewards(stringArray);
			rwdStat.add(rwstat);
		}
		return rwdStat;
	}

	
	/**
	 * @param bucket
	 * @param res
	 * @param filename
	 * @param res2
	 * @return
	 * @throws IOException
	 */
	private static String getServingUrl(String bucket, byte[] res, String filename) throws IOException {
		String url;
		byte[] finalres = res;

		gcsService.createOrReplace(new GcsFilename(bucket, filename),
				new GcsFileOptions.Builder().mimeType("image/jpeg").acl("project-private").build(),
				ByteBuffer.wrap(finalres));
		
		ImagesService imagesService = ImagesServiceFactory.getImagesService();
		/**
		 * Create a fixed dedicated URL that points to the GCS hosted file
		 */ 
		ServingUrlOptions options = ServingUrlOptions.Builder
				.withGoogleStorageFileName("/gs/" + bucket + "/" + filename).imageSize(620).crop(false)
				.secureUrl(false);
		url = imagesService.getServingUrl(options);
		return url;
	}


}
