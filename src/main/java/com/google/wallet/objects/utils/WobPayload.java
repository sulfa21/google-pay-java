package com.google.wallet.objects.utils;

import java.util.ArrayList;
import java.util.List;
import com.google.api.client.json.GenericJson;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.walletobjects.model.LoyaltyClass;
import com.google.api.services.walletobjects.model.LoyaltyObject;
import com.google.gson.Gson;
import com.google.wallet.objects.webservice.WebserviceResponse;


/**
 * Bean to represent the Wob payload object. To add an object to the WobPayload,
 * use the addObject method. This classes uses reflection to determine the
 * correct array to insert into.
 *
 * 
 *
 */
public class WobPayload {
  private List<GenericJson> loyaltyClasses = new ArrayList<GenericJson>();
  private List<GenericJson> loyaltyObjects = new ArrayList<GenericJson>();
  private WebserviceResponse webserviceResponse;
  private transient Gson gson = new Gson();
  private transient JsonFactory jsonFactory = new GsonFactory();

  /**
   * Empty default constructor for Gson deserialization
   */
  public WobPayload() {
  }

  /**
   * Adds the object to the appropriate list
   *
   * @param object
   */
  public void addObject(GenericJson object) {
    object.setFactory(jsonFactory);

    if (LoyaltyObject.class.isAssignableFrom(object.getClass())) {
      addLoyaltyObject(gson.fromJson(object.toString(), GenericJson.class));
    }  else
      throw new IllegalArgumentException("Invalid Object type: "
          + object.getClass());
  }

  public void addLoyaltyObject(GenericJson object) {
    loyaltyObjects.add(object);
  }

  public List<GenericJson> getLoyaltyObjects() {
    return loyaltyObjects;
  }

  public void setLoyaltyObjects(List<GenericJson> loyaltyObject) {
    this.loyaltyObjects = loyaltyObject;
  }

  public WebserviceResponse getResponse() {
    return webserviceResponse;
  }

  public void setResponse(WebserviceResponse resp) {
    this.webserviceResponse = resp;
  }

  public List<GenericJson> getLoyaltyClasses() {
    return loyaltyClasses;
  }

  public void setLoyaltyClasses(List<GenericJson> loyaltyClasses) {
    this.loyaltyClasses = loyaltyClasses;
  }

  public void addLoyaltyClass(GenericJson object) {
    loyaltyClasses.add(object);
  }
}
