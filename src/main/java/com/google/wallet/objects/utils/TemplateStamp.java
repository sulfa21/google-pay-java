package com.google.wallet.objects.utils;

public class TemplateStamp {
	private int xa;
	private int yb;
	private int ya;
	private int xp;
	private int maxStampPerRow;
	private int stampSize;
	
	public TemplateStamp(int xa, int yb, int ya, int xp, int maxStampPerRow, int stampSize) {
		this.xa = xa;
		this.yb = yb;
		this.ya = ya;
		this.xp = xp;
		this.maxStampPerRow = maxStampPerRow;
		this.stampSize = stampSize;
	}
	
	public TemplateStamp(int xa, int ya, int xp, int maxStampPerRow, int stampSize) {
		this.xa = xa;
		this.ya = ya;
		this.xp = xp;
		this.maxStampPerRow = maxStampPerRow;
		this.stampSize = stampSize;
	}
	
	public TemplateStamp() {

	}
	
	public int getYa() {
		return ya;
	}
	
	public void setYa(int ya) {
		this.ya = ya;
	}
	
	public int getXa() {
		return xa;
	}
	
	public void setXa(int xa) {
		this.xa = xa;
	}
	
	public int getYb() {
		return yb;
	}
	
	public void setYb(int yb) {
		this.yb = yb;
	}
	
	public int getXp() {
		return xp;
	}
	
	public void setXp(int xp) {
		this.xp = xp;
	}
	
	public int getMaxStampPerRow() {
		return maxStampPerRow;
	}
	
	public void setMaxStampPerRow(int maxStampPerRow) {
		this.maxStampPerRow = maxStampPerRow;
	}
	
	public int getStampSize() {
		return stampSize;
	}
	
	public void setStampSize(int stampSize) {
		this.stampSize = stampSize;
	}
}
