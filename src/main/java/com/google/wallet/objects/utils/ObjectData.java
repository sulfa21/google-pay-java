package com.google.wallet.objects.utils;

import java.util.List;

/**
 * @author sulfa
 *
 */
public class ObjectData {
	List<RewardStatus> rewardStatus;
	private String lastClaimed;
	private String lastClaimedProduct;
	
	public List<RewardStatus> getRewardStatus() {
		return rewardStatus;
	}
	
	public void setRewardStatus(List<RewardStatus> rewardStatus) {
		this.rewardStatus = rewardStatus;
	}
	
	public String getLastClaimed() {
		return lastClaimed;
	}
	
	public void setLastClaimed(String lastClaimed) {
		this.lastClaimed = lastClaimed;
	}
	
	public String getLastClaimedProduct() {
		return lastClaimedProduct;
	}
	
	public void setLastClaimedProduct(String lastClaimedProduct) {
		this.lastClaimedProduct = lastClaimedProduct;
	}
}
