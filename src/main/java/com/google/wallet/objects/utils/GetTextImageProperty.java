package com.google.wallet.objects.utils;

import java.util.HashMap;

/**
 * @author sulfa
 *
 */
public class GetTextImageProperty {
	private String textUrl;
	private int count;
	HashMap<Integer, String> hmap = new HashMap<Integer, String>();

	public GetTextImageProperty(String textUrl, int count, HashMap<Integer, String> hmap) {
		this.textUrl = textUrl;
		this.count = count;
		this.hmap = hmap;
	}

	public GetTextImageProperty() {

	}

	public String getTextUrl() {
		return textUrl;
	}

	public void setTextUrl(String textUrl) {
		this.textUrl = textUrl;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public HashMap<Integer, String> getHmap() {
		return hmap;
	}

	public void setHmap(HashMap<Integer, String> hmap) {
		this.hmap = hmap;
	}
}
