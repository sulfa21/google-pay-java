package com.google.wallet.objects.utils;

import java.security.interfaces.RSAPrivateKey;

/**
 * Class to define Wallet Object related credentials. These credentials are used
 * with WobUtils to simplify the OAuth token generation and JWT signing.
 *
 * If you're using a key file, specify the privateKeyPath using the first
 * constructor. If you've defined the private key as a base 64 encoded string,
 * convert it to an RSAPrivate Key with WobUtils.getRsaPrivateKey.
 *
 * @author sulfa
 *
 */
public class WobCredentials {
  String serviceAccountId;
  String serviceAccountPrivateKeyPath;
  String applicationName;
  String issuerId;
  RSAPrivateKey serviceAccountPrivateKey;

  public WobCredentials(String serviceAccountId, String privateKeyPath,
      String applicationName, String issuerId) {
    setServiceAccountId(serviceAccountId);
    setServiceAccountPrivateKeyPath(privateKeyPath);
    setApplicationName(applicationName);
    setIssuerId(issuerId);
  }

  public WobCredentials(String serviceAccountId, RSAPrivateKey privateKey,
      String applicationName, String issuerId) {
    setServiceAccountId(serviceAccountId);
    setServiceAccountPrivateKey(privateKey);
    setApplicationName(applicationName);
    setIssuerId(issuerId);
  }

  public RSAPrivateKey getServiceAccountPrivateKey() {
    return serviceAccountPrivateKey;
  }

  public void setServiceAccountPrivateKey(RSAPrivateKey serviceAccountPrivateKey) {
    this.serviceAccountPrivateKey = serviceAccountPrivateKey;
  }

  public String getServiceAccountId() {
    return serviceAccountId;
  }

  public void setServiceAccountId(String serviceAccountId) {
    this.serviceAccountId = serviceAccountId;
  }

  public String getServiceAccountPrivateKeyPath() {
    return serviceAccountPrivateKeyPath;
  }

  public void setServiceAccountPrivateKeyPath(String serviceAccountPrivateKey) {
    this.serviceAccountPrivateKeyPath = serviceAccountPrivateKey;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public String getIssuerId() {
    return issuerId;
  }

  public void setIssuerId(String issuerId) {
    this.issuerId = issuerId;
  }
}
