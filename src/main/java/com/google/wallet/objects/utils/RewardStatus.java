package com.google.wallet.objects.utils;

/**
 * @author sulfa
 *
 */
public class RewardStatus {
	private int milestone;
	private String status;
	private String[] rewards;
	
	public RewardStatus(int milestone, String status, String[] rewards) {
		this.milestone = milestone;
		this.status = status;
		this.rewards = rewards;
	}
	
	public RewardStatus() {
		
	}
	
	public String[] getRewards() {
		return rewards;
	}
	
	public void setRewards(String[] rewards) {
		this.rewards = rewards;
	}
	
	public int getMilestone() {
		return milestone;
	}
	
	public void setMilestone(int milestone) {
		this.milestone = milestone;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
